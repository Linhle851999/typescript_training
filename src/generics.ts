// generics

type strArra = Array<string>;

type numArr = Array<number>;

const last = (arr: Array<number>) => arr[arr.length - 1];

const l1 = last([1, 2, 3]);
console.log(l1);

const l2 = last([1]);
console.log(l2);

const lastT = <T>(arr: Array<T>) => arr[arr.length - 1];

const l3 = lastT([1, 2, 3]);
console.log(l3);
const l5 = lastT<string>(["a", "b"]);

const makeArr = (a: number) => [a];
const m = makeArr(5);

console.log(m);

// const m2 = makeArr("a");
const makeArrT = <T>(x: T) => [x];
const m3 = makeArrT(5);

const makeArrXY = <X, Y>(a: X, b: Y) => [a, b];
console.log(makeArrXY(5, 6));

const makeTuple = <X, Y>(x: X, y: Y): [X, Y] => [x, y];
const m7 = makeTuple("x", 5);
const m8 = makeTuple("a", "b");

const m9 = makeTuple<string, number>("a", 23);
const m10 = makeTuple<string | null, string>("a", "a");

const makeTupleWithDefault = <X, Y = number>(x: X, y: Y): [X, Y] => [x, y];
const m11 = makeTupleWithDefault<string | null>("stirng", 5);

// generics extends

const makeFullName = (obj) => ({
  ...Object,
  fullName: `${obj.firsName} ${obj.lastName}`,
});

const makeFullNameConstraint = (obj: {
  firsName: string;
  lastName: string;
}) => ({
  ...obj,
  fullName: `${obj.firsName} ${obj.lastName}`,
});
const n1 = makeFullNameConstraint({ firsName: "linh", lastName: "le" });
//const n2 = makeFullNameConstraint({ firsName: "linh", lastName: "le" });

const makeFullNameConstraintGenerisc = <
  T extends { firsName: string; lastName: string }
>(
  obj: T
) => ({
  ...obj,
  fullName: `${obj.firsName} ${obj.lastName}`,
});

const m5 = makeFullNameConstraintGenerisc({
  firsName: "linh",
  lastName: "le",
  age: 30,
});

const addNewEmployee = (employee: object) => {
  const uid = Math.floor(Math.random() * 100);
  return {
    ...employee,
    uid,
  };
};
const employee = addNewEmployee({ name: "herry", age: 30 });
console.log(employee);
//console.log(employee.name)

const addNewE = <T extends object>(employee: T) => {
  const uid = Math.floor(Math.random() * 100);
  return {
    ...employee,
    uid,
  };
};
const empTwo = addNewE({ name: "name", age: 20, male: true });
console.log(empTwo);
console.log(empTwo.name);

const addNewEmployeeTwo = <T extends { name: string }>(employee: T) => {
  const uid = Math.floor(Math.random() * 100);
  return {
    ...employee,
    uid,
  };
};

const empThree = addNewEmployeeTwo({ name: "linh", age: 30 });

//generics in interface

interface Resource<T> {
  uid: number;
  name: string;
  data: T;
}
type NumberResource = Resource<number[]>;

const numbers: NumberResource = {
  uid: 2,
  name: "linh",
  data: [1, 2, 3],
};
const ResourceOne: Resource<string> = {
  uid: 1,
  name: "linh",
  data: "hello",
};

const ResourceTwo: Resource<object> = {
  uid: 1,
  name: "linh",
  data: { name: "linh" },
};

const ResourceThree: Resource<string[]> = {
  uid: 1,
  name: "linh",
  data: ["css", "html"],
};
