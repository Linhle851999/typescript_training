// union type and enum type

// union

let size: string | number;

size = "xl";
size = 30;

let size1: string | string[];

//enum

enum Colors {
  red = "red",
  black = "black",
  green = "green",
}

let color = Colors.red;
