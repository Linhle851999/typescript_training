// function

const square = (side: number) => side * 2;

console.log(square(3));

let greet: Function;

greet = () => {
  console.log("hello");
};

greet();
// ? không bắt buộc phải truyền vào
const add = (a: number, b: number, c?: number | string) => {
  console.log(a + b);
  console.log(c);
};
add(1, 2, 5);

const adDefault = (a: number, b: number, c: number | string = 10) => {
  console.log("sum", a + b);
};
adDefault(1, 3);

const minus = (a: number, b: number): number => a + b;

// function type

// function return value

const sum = (sum1: number, sum2: number): number => {
  return sum1 + sum2;
};
sum(1, 2);

const a = (string1: string, string2: string) => {
  return string1 + string2;
};

type functionType = (number1: number, number2: number) => number;

const sum3: functionType = (num1, num2) => {
  return num1 + num2;
};
sum3();

// function not return value

const hello = (): void => {
  console.log("hello");
};
