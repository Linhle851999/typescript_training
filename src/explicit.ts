// explicit types
let myNames: string = "name";
let age: number;
let isAuthenticated: boolean;
myNames = "linh";

isAuthenticated = false;

let students: string[] = ["nam", "linh"];
students.push("lan");

let mixed: (string | number | boolean)[];
mixed = ["herry", 5, true];
mixed.push(6);
mixed.push("lan");
mixed.push(false);

let id: string | number;
id = 123;
id = "abc";

let hobby: "book" | "music";
hobby = "book";

let person: object;
person = { name: "linh", age: 30 };
person = [];

let student: {
  name: string;
  age: number;
  isGood: boolean;
};

student = {
  name: "linh",
  age: 20,
  isGood: true,
};
