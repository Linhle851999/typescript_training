// array and object

let names = ["linh", "hoang", "james"];
names.push("son");

let number = [1, 2, 3];
number[3] = 10;

let mixed = [1, "herry", false];

mixed.push(5);
mixed.push("herrr");
mixed[3] = true;

//object
let person = {
  name: "herry",
  age: 25,
  isStudent: false,
};
person.name = "Linh";
person.age = 20;

person = {
  name: "ha",
  age: 25,
  isStudent: true,
};
