// generics
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var last = function (arr) { return arr[arr.length - 1]; };
var l1 = last([1, 2, 3]);
console.log(l1);
var l2 = last([1]);
console.log(l2);
var lastT = function (arr) { return arr[arr.length - 1]; };
var l3 = lastT([1, 2, 3]);
console.log(l3);
var l5 = lastT(["a", "b"]);
var makeArr = function (a) { return [a]; };
var m = makeArr(5);
console.log(m);
// const m2 = makeArr("a");
var makeArrT = function (x) { return [x]; };
var m3 = makeArrT(5);
var makeArrXY = function (a, b) { return [a, b]; };
console.log(makeArrXY(5, 6));
var makeTuple = function (x, y) { return [x, y]; };
var m7 = makeTuple("x", 5);
var m8 = makeTuple("a", "b");
var m9 = makeTuple("a", 23);
var m10 = makeTuple("a", "a");
var makeTupleWithDefault = function (x, y) { return [x, y]; };
var m11 = makeTupleWithDefault("stirng", 5);
// generics extends
var makeFullName = function (obj) { return (__assign(__assign({}, Object), { fullName: "".concat(obj.firsName, " ").concat(obj.lastName) })); };
var makeFullNameConstraint = function (obj) { return (__assign(__assign({}, obj), { fullName: "".concat(obj.firsName, " ").concat(obj.lastName) })); };
var n1 = makeFullNameConstraint({ firsName: "linh", lastName: "le" });
//const n2 = makeFullNameConstraint({ firsName: "linh", lastName: "le" });
var makeFullNameConstraintGenerisc = function (obj) { return (__assign(__assign({}, obj), { fullName: "".concat(obj.firsName, " ").concat(obj.lastName) })); };
var m5 = makeFullNameConstraintGenerisc({
    firsName: "linh",
    lastName: "le",
    age: 30
});
var addNewEmployee = function (employee) {
    var uid = Math.floor(Math.random() * 100);
    return __assign(__assign({}, employee), { uid: uid });
};
var employee = addNewEmployee({ name: "herry", age: 30 });
console.log(employee);
//console.log(employee.name)
var addNewE = function (employee) {
    var uid = Math.floor(Math.random() * 100);
    return __assign(__assign({}, employee), { uid: uid });
};
var empTwo = addNewE({ name: "name", age: 20, male: true });
console.log(empTwo);
console.log(empTwo.name);
var addNewEmployeeTwo = function (employee) {
    var uid = Math.floor(Math.random() * 100);
    return __assign(__assign({}, employee), { uid: uid });
};
var empThree = addNewEmployeeTwo({ name: "linh", age: 30 });
var numbers = {
    uid: 2,
    name: "linh",
    data: [1, 2, 3]
};
var ResourceOne = {
    uid: 1,
    name: "linh",
    data: "hello"
};
var ResourceTwo = {
    uid: 1,
    name: "linh",
    data: { name: "linh" }
};
var ResourceThree = {
    uid: 1,
    name: "linh",
    data: ["css", "html"]
};
